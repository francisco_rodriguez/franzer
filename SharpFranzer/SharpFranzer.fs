﻿namespace SharpFranzer
open Robocode

type Franzer() = 
    inherit Robot()
    override this.Run () = 
        let mutable continueLooping = true 
        this.Heading - 90.0 |> this.TurnLeft 
        this.TurnGunRight 90.0
        while continueLooping do 
            this.Ahead 5000.0
            this.TurnRight 90.0
     override this.OnScannedRobot e =
         this.Fire(1.0)
           

